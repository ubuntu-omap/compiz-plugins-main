#!/bin/bash

export PACK=compiz-plugins-main
export GITPACK=`echo ${PACK} | cut -b 8-`
export CURRENT_PLUGINS=`cat .gitmodules | grep "path =" | cut -b 9-`

echo "Removing all the plugins"
for CURRENT_PLUGIN in ${CURRENT_PLUGINS}; do
        rm -rfv ${CURRENT_PLUGIN}
done

bzr commit -m "Sync - Remove Plugins"
echo "Syncing in changes from upstream - cloning " ${GITPACK}
git clone git://git.compiz.org/compiz/${GITPACK} ${PACK}-tmp
mv ${PACK}-tmp/.gitmodules .
mv ${PACK}-tmp/* .
rm -rf ${PACK}-tmp

PLUGINS=`cat .gitmodules | grep "path =" | cut -b 9-`

for PLUGIN in ${PLUGINS}; do
	rm -rfv ${PLUGIN}
	echo "Branching bzr plugin compiz-${PLUGIN}-plugin"
	bzr branch lp:~compiz-team/compiz-${PLUGIN}-plugin/oneiric
	mv oneiric ${PLUGIN}
	cd ${PLUGIN}
	rm -rf .bzr
	cd ..
	bzr add ${PLUGIN}
done

bzr commit -m "Sync in changes from upstream"
echo "Done"
